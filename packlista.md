# Packlista

https://www.utsidan.se/forum/showthread.php?t=85871

## Utrustning
* [ ] Ryggsäck
* [ ] Regnskydd t. ryggsäck
* [ ] Sovsäck
* [ ] Liggunderlag
* [ ] Pump till madrass (om nödvändigt)
* [ ] Tält (om nödvändigt)

## Äta
* [ ] Frystorkat 4 Lunch + 2 Middag
* [ ] "Riktig mat" 2 Middag
* [ ] Frukost
* [ ] Disksvamp
* [ ] Bestick
* [ ] Tallrik
* [ ] Kåsa
* [ ] Vattenflaska
* [ ] Resorb

## Från apoteket
* [ ] Myggmedel
* [ ] Skavsårsplåster
* [ ] Första hjälpen
* [ ] Solskyddsfaktor

## På fötterna
* [ ] Kängor
* [ ] Löparskor
* [ ] Strumpor (inner och ytter)

## Kläder
* [ ] Mössa
* [ ] Buff
* [ ] Keps / Hatt
* [ ] Vantar
* [ ] Skaljacka / Regnjacka
* [ ] Skalbyxor /Regnbyxor
* [ ] Fleece-tröja
* [ ] Flanellskjorta
* [ ] Byxor
* [ ] Shorts
* [ ] Underställ
* [ ] T-shirts
* [ ] Kalsonger

## Diverse
* [ ] Kniv
* [ ] Fiskespö (spinn och fluga)
* [ ] Rulle (spinn och fluga)
* [ ] Drag (spinn samt flugor)
* [ ] Flugfiskeprylar (tafs, flytmedel för tafs)
* [ ] Kikare
* [ ] Pannlampa (midnattssol lol)
* [ ] Nål och tråd
* [ ] Tändstickor
* [ ] Karta
* [ ] Myggnät för ansikte
* [ ] Solglasögon
* [ ] Öronproppar
* [ ] Tvål
* [ ] Tandkräm
* [ ] Tandborste
* [ ] Handduk

## Bra frystorkat:
* Pasta bolognese
* Svinekött
* Pasta lax
* Kremet pasta
* Storfegryta
* (Pulled pork)
